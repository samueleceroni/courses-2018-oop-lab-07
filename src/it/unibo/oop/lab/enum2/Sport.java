/**
 * 
 */
package it.unibo.oop.lab.enum2;

/**
 * Represents an enumeration for declaring sports.
 * 
 * 1) You must add a field keeping track of the number of members each team is
 * composed of (1 for individual sports)
 * 
 * 2) A second field will keep track of the name of the sport.
 * 
 * 3) A third field, of type Place, will allow to define if the sport is
 * practiced indoor or outdoor
 * 
 */
public enum Sport {

    /*
     * 
     * 
     * Declare the following sports:
     * 
     * - basket
     * 
     * - volley
     * 
     * - tennis
     * 
     * - bike
     * 
     * - F1
     * 
     * - motogp
     */
	BASKET(6, "basket", Place.INDOOR),
	SOCCER(11, "soccer", Place.OUTDOOR),
	TENNIS(1, "tennis", Place.INDOOR),
	BIKE(1, "bike", Place.OUTDOOR),
	F1(1, "f1", Place.OUTDOOR),
	MOTOGP(1, "motogp", Place.OUTDOOR),
	VOLLEY(5, "volley", Place.INDOOR);

    /*
     * 
     * 
     * [FIELDS]
     * 
     * Declare required fields
     */
	private String name;
	private int membersNumber;
	private Place location;

    /*
     * 
     * 
     * [CONSTRUCTOR]
     * 
     * Define a constructor like this:
     * 
     * - Sport(final Place place, final int noTeamMembers, final String actualName)
     */
	private Sport(int membersNumber, String name, Place location) {
		this.name = name;
		this.membersNumber = membersNumber;
		this.location = location;
	}
	
    /*
     * TODO
     * 
     * [METHODS] To be defined
     * 
     * 
     * 1) public boolean isIndividualSport()
     * 
     * Must return true only if called on individual sports
     * 
     * 
     * 2) public boolean isIndoorSport()
     * 
     * Must return true in case the sport is practices indoor
     * 
     * 
     * 3) public Place getPlace()
     * 
     * Must return the place where this sport is practiced
     * 
     * 
     * 4) public String toString()
     * 
     * Returns the string representation of a sport
     */
	public boolean isIndividualSport() {
		return this.membersNumber == 1;
	}
	
	public boolean isIndoorSport() {
		return this.location == Place.INDOOR;
	}
	
	public Place getPlace() {
		return this.location;
	}
	
	public String toString() {
		return "[" + this.name + ": " + this.location.toString() +", " + (Integer.toString(this.membersNumber) + "].");
	}
	
}

































